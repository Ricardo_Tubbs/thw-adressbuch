import { defineStore } from 'pinia';
import { ref } from 'vue';
import { Notify } from 'quasar';
export const useGeoDataStore = defineStore('GeoData', () => {
  const geolocation = ref<number[] | null>(null);
  const watchId = ref<number | null>(null);
  const watchGeoLocation = () => {
    console.log('watchGeoLocation');
    navigator.geolocation.getCurrentPosition(
      (position) => {
        geolocation.value = [
          position.coords.latitude,
          position.coords.longitude,
        ];
        UpdateGeolocation();
        console.log('geolocation', position);
      },
      (error) => {
        console.log(error);
        if (error.PERMISSION_DENIED) {
          console.log('User denied the request for Geolocation.');
          Notify.create({
            type: 'negative',
            position: 'center',
            timeout: 3000,
            message:
              'Benutzer oder Browser hat die Erlaubnis für die Geolocation/GPS abgelehnt',
          });
        }
      },
      { maximumAge: 0, timeout: 5000, enableHighAccuracy: false }
    );
  };

  const UpdateGeolocation = () => {
    if (watchId.value === null) {
      watchId.value = navigator.geolocation.watchPosition((position) => {
        geolocation.value = [
          position.coords.latitude,
          position.coords.longitude,
        ];
      });
    }
  };

  return { geolocation, watchGeoLocation };
});
