export interface Leader {
  name: string;
  picture: string;
  title: string;
}

export interface Teileinheit {
  name: string;
  link: string;
  extra: string;
}

export interface Unit {
  name: string;
  link: string;
  teileinheit: Teileinheit[];
}

export interface ScrapedDienststellenGeo extends ScrapedDienststellen {
  distance_to_geolocation?: number;
}

export interface OsmData {
  coordinates: number[];
  osm_id: string | number;
  osm_type: string;
  housenumber: string;
  postcode: string;
  street: string;
  email: string;
  fax: string;
  phone: string;
  website: string;
}

export interface ScrapedDienststellen {
  street: string;
  code: string;
  oeId: number;
  website: string;
  zip: string;
  city: string;
  region: string;
  phone: string;
  fax: string;
  email: string;
  takzeich: string;
  name: string;
  coordinates: number[];
  leader: Leader[];
  osm_data: OsmData;
  link: string;
  type: string;
  parents: string[];
  units: Unit[];
}

export interface Commit {
  id: string;
  short_id: string;
  created_at: string;
  parent_ids?: string[] | null;
  title: string;
  message: string;
  author_name: string;
  author_email: string;
  authored_date: string;
  committer_name: string;
  committer_email: string;
  committed_date: string;
  web_url: string;
}
