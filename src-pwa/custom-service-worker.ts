import { GetUpdatedPayload } from './../shared/UpdateScrapedData';
/*
 * This file (which will be your service worker)
 * is picked up by the build system ONLY if
 * quasar.config.js > pwa > workboxPluginMode is set to "InjectManifest"
 */

interface PBGEvent extends Event {
  tag: string;
  waitUntil: (any: any) => null;
}

import { precacheAndRoute } from 'workbox-precaching';

// Use with precache injection
precacheAndRoute((self as any).__WB_MANIFEST);

console.log('PWA Losaded SW');
self.addEventListener('periodicsync', async (event_in) => {
  console.log('SW: Recieved PBG Sync Event');
  const event = event_in as unknown as PBGEvent;
  if (event.tag === 'content-sync') {
    event.waitUntil(GetUpdatedPayload(true));
  }
});
self.addEventListener('install', function (event) {
  // The promise that skipWaiting() returns can be safely ignored.
  // @ts-expect-error ts-migrate(2339) Property 'skipWaiting' does not exist on type 'EventTarget'.
  self.skipWaiting();
  console.log('SW: Installed skipWaiting');

  // Perform any other actions required for your
  // service worker to install, potentially inside
  // of event.waitUntil();
});
