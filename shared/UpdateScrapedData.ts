import { ScrapedDienststellen } from './../src/stores/StoreTypes.d';
import * as LocalStorage from 'localforage';
import { Commit } from 'src/stores/StoreTypes';
const data_payload_key = 'data_payload';
const data_version_key = 'data_commit_version_info';
const fetch_timeout = 10000;

export const GetUpdatedPayload = async (
  saveLocal = true
): Promise<ScrapedDienststellen[] | null> => {
  console.log('Shared: Run Update Check and Set');
  const updateNeeded = await CheckUpdateNeeded();
  let ScrapedDienststellen: ScrapedDienststellen[] | null = null;
  if (updateNeeded) {
    ScrapedDienststellen = await GetLatestPayload();
    if (saveLocal) {
      await LocalStorage.setItem(data_payload_key, ScrapedDienststellen);
    }
    return ScrapedDienststellen;
  } else {
    return await LocalStorage.getItem(data_payload_key);
  }
};

export const GetLatestCommitInfoFromStorage =
  async (): Promise<Commit | null> => {
    const commitInfo = await LocalStorage.getItem(data_version_key);
    if (commitInfo) {
      return commitInfo as Commit;
    } else {
      return null;
    }
  };

export const CheckUpdateNeeded = async (): Promise<boolean> => {
  let currentCommit: Commit | null = null;
  // IF version not exist, create it
  // It is always a Update Needed when no Version Data exists
  if (!(await LocalStorage.getItem(data_version_key))) {
    currentCommit = await GetLatestCommitInfo();
    await LocalStorage.setItem(data_version_key, currentCommit);
    return true;
  } else {
    currentCommit = await LocalStorage.getItem(data_version_key);
  }
  if (!LocalStorage.getItem(data_payload_key)) return true;

  // Check If Current Data UptoDate
  const remoteCommit = await GetLatestCommitInfo(currentCommit);

  // If remoteCommit is null, we can't check if it is newer
  if (!remoteCommit) {
    console.log('Shared: No Remote Commit Found');
    return false;
  }

  if (remoteCommit?.short_id === currentCommit?.short_id) return false;
  // The Local and the Remote Commit are not the same, so set Local Commit to the Remote
  await LocalStorage.setItem(data_version_key, remoteCommit);
  return true;
};

const GetLatestCommitInfo = async (
  currentCommit?: Commit | null
): Promise<Commit | null> => {
  try {
    const controller = new AbortController();
    const id = setTimeout(() => controller.abort(), fetch_timeout);
    // eslint-disable-next-line camelcase
    const resp = await (
      await fetch(
        // eslint-disable-next-line camelcase
        `https://gitlab.com/api/v4/projects/25364116/repository/commits?since=${
          currentCommit?.committed_date || ''
        }`,
        {
          method: 'GET',
          headers: {},
          credentials: 'same-origin',
          signal: controller.signal,
        }
      )
    ).text();
    clearTimeout(id);
    const newCommits: Commit[] = JSON.parse(resp) as Commit[];
    return newCommits[0] || currentCommit;
  } catch (error) {
    console.log('er', error);
    return null;
  }
};

const GetLatestPayload = async (): Promise<ScrapedDienststellen[] | null> => {
  try {
    const controller = new AbortController();
    const id = setTimeout(() => controller.abort(), fetch_timeout);
    const resp = await (
      await fetch(
        'https://gitlab.com/api/v4/projects/25364116/repository/files/dienststellen.json/raw',
        {
          method: 'GET',
          headers: {},
          credentials: 'same-origin',
          signal: controller.signal,
        }
      )
    ).text();
    clearTimeout(id);
    const newPayload: ScrapedDienststellen[] = JSON.parse(
      resp
    ) as ScrapedDienststellen[];
    return newPayload;
  } catch (error) {
    console.log('er', error);
    return null;
  }
};

const geoJsonUrl =
  'https://raw.githubusercontent.com/isellsoap/deutschlandGeoJSON/main/2_bundeslaender/4_niedrig.geo.json';

// Save geoJson to LocalStorage as geoJson
export const SaveGeoJson = async (): Promise<void> => {
  const geoJson = await fetch(geoJsonUrl).then((res) => res.json());
  await LocalStorage.setItem('geoJson', geoJson);
  return geoJson;
};

// Get geoJson from LocalStorage if not exist Save it
export const GetGeoJson = async (): Promise<any> => {
  let geoJson = await LocalStorage.getItem('geoJson');
  if (!geoJson) {
    geoJson = await SaveGeoJson();
  }
  return geoJson;
};
